void main() {
  var app=Application('Murimi','Agriculture','Mukondleteri Dumela',2021);
  print(app.appName);
  print(app.appCategory);
  print(app.appDeveloper);
  print(app.appYear);
}

class Application {
  //Instance variables
  String appName='';
  String appCategory='';
  String appDeveloper='';
  int appYear=2021;
  
  Application(String appName, String appCategory, String appDeveloper, int appYear)
  {
    this.appName = appName;
    this.appCategory = appCategory;
    this.appDeveloper = appDeveloper;
    this.appYear=appYear;
  }
}
